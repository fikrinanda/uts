package nanda.fikri.socialmedia

import android.Manifest
import android.app.AlertDialog
import android.app.ProgressDialog
import android.content.ContentValues
import android.content.DialogInterface
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import android.text.TextUtils
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.ActionBar
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.database.*
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.StorageReference
import kotlinx.android.synthetic.main.activity_add_post.*
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.HashMap

class AddPostActivity : AppCompatActivity(), View.OnClickListener {

    var firebaseAuth: FirebaseAuth? = null
    var user: FirebaseUser? = null
    var db: FirebaseDatabase? = null
    var dbref: DatabaseReference? = null
    var CAMERA_REQUEST_CODE = 100
    var STORAGE_REQUEST_CODE = 200
    var IMAGE_PICK_GALLERY_REQUEST_CODE = 300
    var IMAGE_PICK_CAMERA_REQUEST_CODE = 400
    var cameraPermissions: Array<String> = Array(5) { "" }
    var storagePermissions: Array<String> = Array(5) { "" }
    var image_uri: Uri? = null
    var values: ContentValues? = null
    var stref: StorageReference? = null
    lateinit var hashmap: HashMap<String, String>
    var name: String? = null
    var email: String? = null
    var uid: String? = null
    var dp: String? = null
    lateinit var progressDialog: ProgressDialog


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_post)

        val actionBar: ActionBar? = this.supportActionBar
        actionBar?.setTitle("Add New Post")
        actionBar?.setDisplayHomeAsUpEnabled(true)
        actionBar?.setDisplayShowHomeEnabled(true)

        firebaseAuth = FirebaseAuth.getInstance()
        checkUserStatus()

        actionBar?.setSubtitle(email)

        user = firebaseAuth?.currentUser
        db = FirebaseDatabase.getInstance()
        dbref = db?.getReference("Users")

        var query = dbref?.orderByChild("email")?.equalTo(user?.email)
        query?.addValueEventListener(object : ValueEventListener {
            override fun onDataChange(snapshot: DataSnapshot) {

                for (ds in snapshot.children) {
                    name = "" + ds.child("name").getValue()
                    email = "" + ds.child("email").getValue()
                    dp = "" + ds.child("image").getValue()
                }

            }

            override fun onCancelled(error: DatabaseError) {

            }
        })

        pUploadBtn.setOnClickListener(this)
        pImageIv.setOnClickListener(this)

        cameraPermissions =
            arrayOf(Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE)
        storagePermissions = arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE)

        hashmap = HashMap()
        values = ContentValues()

        stref = FirebaseStorage.getInstance().reference
        progressDialog = ProgressDialog(this)
    }

    override fun onStart() {
        super.onStart()
        checkUserStatus()
    }

    override fun onResume() {
        super.onResume()
        checkUserStatus()
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return super.onSupportNavigateUp()
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_main, menu)
        menu?.findItem(R.id.action_add_post)?.setVisible(false)
        menu?.findItem(R.id.action_search)?.setVisible(false)
        return super.onCreateOptionsMenu(menu)
    }

    fun checkUserStatus() {
        var user = firebaseAuth?.currentUser
        if (user != null) {
            email = user.email
            uid = user.uid
        } else {
            startActivity(Intent(this, MainActivity::class.java))
            finish()
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.action_logout -> {
                firebaseAuth?.signOut()
                checkUserStatus()
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.pUploadBtn -> {
                var ttl = pTitleEt.text.toString().trim()
                var description = pDescriptionEt.text.toString().trim()
                if (TextUtils.isEmpty(ttl)) {
                    Toast.makeText(this, "Enter title...", Toast.LENGTH_SHORT).show()
                    return
                }
                if (TextUtils.isEmpty(description)) {
                    Toast.makeText(this, "Enter description...", Toast.LENGTH_SHORT).show()
                    return
                }

                if (image_uri == null) {
                    uploadData(ttl, description, "noImage")
                } else {
                    uploadData(ttl, description, image_uri.toString())
                }
            }
            R.id.pImageIv -> {
                showImagePickDialog()
            }
        }
    }

    fun showImagePickDialog() {
        var c = AlertDialog.Builder(this)
        val options = arrayOf("Camera", "Gallery")
        c.setTitle("Pick Image From")
        c.setItems(options, DialogInterface.OnClickListener { dialogInterface, i ->
            if (i == 0) {
                if (!checkCameraPermission()) {
                    requestCameraPermission()
                } else {
                    pickFromCamera()
                }
            } else if (i == 1) {
                if (!checkStoragePermission()) {
                    requestStoragePermission()
                } else {
                    pickFromGallery()
                }
            }
        })
        c.create().show()
    }

    fun checkStoragePermission(): Boolean {
        var result =
            ContextCompat.checkSelfPermission(
                this!!,
                Manifest.permission.WRITE_EXTERNAL_STORAGE
            ) == (PackageManager.PERMISSION_GRANTED)
        return result
    }

    fun requestStoragePermission() {
        requestPermissions(storagePermissions, STORAGE_REQUEST_CODE)
    }

    fun checkCameraPermission(): Boolean {
        var result =
            ContextCompat.checkSelfPermission(
                this!!,
                Manifest.permission.CAMERA
            ) == (PackageManager.PERMISSION_GRANTED)

        var result1 =
            ContextCompat.checkSelfPermission(
                this!!,
                Manifest.permission.WRITE_EXTERNAL_STORAGE
            ) == (PackageManager.PERMISSION_GRANTED)
        return result && result1
    }

    fun requestCameraPermission() {
        requestPermissions(cameraPermissions, CAMERA_REQUEST_CODE)
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        when (requestCode) {
            CAMERA_REQUEST_CODE -> {
                if (grantResults.size > 0) {
                    var cameraAccepted = grantResults[0] == PackageManager.PERMISSION_GRANTED
                    var writeStorageAccepted = grantResults[1] == PackageManager.PERMISSION_GRANTED
                    if (cameraAccepted && writeStorageAccepted) {
                        pickFromCamera()
                    } else {
                        Toast.makeText(
                            this!!,
                            "Please enable camera & storage permission",
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                }
            }
            STORAGE_REQUEST_CODE -> {
                if (grantResults.size > 0) {
                    var writeStorageAccepted = grantResults[1] == PackageManager.PERMISSION_GRANTED
                    if (writeStorageAccepted) {
                        pickFromGallery()
                    } else {
                        Toast.makeText(
                            this!!,
                            "Please enable storage permission",
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                }
            }
        }

        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
    }

    fun pickFromCamera() {
        values?.put(MediaStore.Images.Media.TITLE, "Temp Pic")
        values?.put(MediaStore.Images.Media.DESCRIPTION, "Temp Description")
        image_uri =
            this!!.contentResolver?.insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values)
        var cameraIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, image_uri)
        startActivityForResult(cameraIntent, IMAGE_PICK_CAMERA_REQUEST_CODE)
    }

    fun pickFromGallery() {
        var galleryIntent = Intent(Intent.ACTION_PICK)
        galleryIntent.setType("image/*")
        startActivityForResult(galleryIntent, IMAGE_PICK_GALLERY_REQUEST_CODE)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (resultCode == RESULT_OK) {
            if (requestCode == IMAGE_PICK_GALLERY_REQUEST_CODE) {
                image_uri = data?.data
                pImageIv.setImageURI(image_uri)
            }
            if (requestCode == IMAGE_PICK_CAMERA_REQUEST_CODE) {
                pImageIv.setImageURI(image_uri)
            }
        }
        super.onActivityResult(requestCode, resultCode, data)
    }

    fun uploadData(title: String, description: String, uri: String) {
        progressDialog.setMessage("Publishing post...")
        progressDialog.show()

        var time = System.currentTimeMillis()
        var sdf = SimpleDateFormat("dd/M/yyyy hh:mm:ss")
        var currentDate = sdf.format(Date())
        var filePathName = "Posts/" + "post_" + time

        if (!uri.equals("noImage")) {
            var u = Uri.parse(uri)
            var ref = FirebaseStorage.getInstance().getReference().child(filePathName)
            ref.putFile(u)!!
                .addOnSuccessListener {

                    val result = it.metadata!!.reference!!.downloadUrl;
                    result.addOnSuccessListener {

                        hashmap.put("uid", uid!!)
                        hashmap.put("uName", name!!)
                        hashmap.put("uEmail", email!!)
                        hashmap.put("uDp", dp!!)
                        hashmap.put("pId", time.toString())
                        hashmap.put("pTitle", title)
                        hashmap.put("pDesc", description)
                        hashmap.put("pImage", it.toString())
                        hashmap.put("pTime", currentDate)

                        var ref = FirebaseDatabase.getInstance().getReference("Posts")
                        ref.child(time.toString()).setValue(hashmap)
                            .addOnSuccessListener {
                                progressDialog.dismiss()
                                Toast.makeText(this, "Post published...", Toast.LENGTH_SHORT)
                                    .show()
                                pTitleEt.setText("")
                                pDescriptionEt.setText("")
                                pImageIv.setImageURI(null)
                                image_uri = null
                            }
                            .addOnFailureListener {
                                progressDialog.dismiss()
                                Toast.makeText(this, "" + it.message, Toast.LENGTH_SHORT).show()
                            }


                    }

                }
                .addOnFailureListener {
                    progressDialog.dismiss()
                    Toast.makeText(this, "" + it.message, Toast.LENGTH_SHORT).show()
                }
        } else {

            hashmap.put("uid", uid!!)
            hashmap.put("uName", name!!)
            hashmap.put("uEmail", email!!)
            hashmap.put("uDp", dp!!)
            hashmap.put("pId", time.toString())
            hashmap.put("pTitle", title)
            hashmap.put("pDesc", description)
            hashmap.put("pImage", "noImage")
            hashmap.put("pTime", currentDate)

            var ref = FirebaseDatabase.getInstance().getReference("Posts")
            ref.child(time.toString()).setValue(hashmap)
                .addOnSuccessListener {
                    progressDialog.dismiss()
                    Toast.makeText(this, "Post published...", Toast.LENGTH_SHORT)
                        .show()
                    pTitleEt.setText("")
                    pDescriptionEt.setText("")
                    pImageIv.setImageURI(null)
                    image_uri = null
                }
                .addOnFailureListener {
                    progressDialog.dismiss()
                    Toast.makeText(this, "" + it.message, Toast.LENGTH_SHORT).show()
                }


        }
    }
}