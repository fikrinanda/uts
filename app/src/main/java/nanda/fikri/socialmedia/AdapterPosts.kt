package nanda.fikri.socialmedia

import android.app.ProgressDialog
import android.content.Context
import android.content.Intent
import android.view.*
import android.widget.*
import androidx.recyclerview.widget.RecyclerView
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import com.google.firebase.storage.FirebaseStorage
import com.squareup.picasso.Picasso

class AdapterPosts : RecyclerView.Adapter<AdapterPosts.MyHolder> {

    var context: Context
    var postList: List<ModelPost>
    var myUid: String

    constructor(context: Context, postList: List<ModelPost>) {
        this.context = context
        this.postList = postList
        myUid = FirebaseAuth.getInstance().currentUser!!.uid
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyHolder {
        val v = LayoutInflater.from(context).inflate(R.layout.row_posts, parent, false)
        return MyHolder(v)
    }

    override fun onBindViewHolder(holder: MyHolder, position: Int) {
        var uid = postList.get(position).uid
        var uEmail = postList.get(position).uEmail
        var uName = postList.get(position).uName
        var uDp = postList.get(position).uDp
        var pId = postList.get(position).pId
        var pTitle = postList.get(position).pTitle
        var pDescription = postList.get(position).pDesc
        var pImage = postList.get(position).pImage
        var pTimeStamp = postList.get(position).pTime

        holder.uNameTv.setText(uName)
        holder.pTimeTv.setText(pTimeStamp)
        holder.pTitleTv.setText(pTitle)
        holder.pDescriptionTv.setText(pDescription)

        try {
            Picasso.get().load(uDp).placeholder(R.drawable.ic_default_img)
                .into(holder.uPictureIv)
        } catch (e: Exception) {

        }

        if (pImage.equals("noImage")) {
            holder.pImageIv.visibility = View.GONE
        } else {
            holder.pImageIv.visibility = View.VISIBLE
            try {
                Picasso.get().load(pImage).placeholder(R.drawable.ic_default_img)
                    .into(holder.pImageIv)
            } catch (e: Exception) {

            }
        }

        holder.moreBtn.setOnClickListener {
            showMoreOptions(holder.moreBtn, uid, myUid, pId, pImage)
        }

        holder.likeBtn.setOnClickListener {
            Toast.makeText(context, "Like", Toast.LENGTH_SHORT).show()
        }

        holder.profileLayout.setOnClickListener {
            var intent = Intent(context, ThereProfileActivity::class.java)
            intent.putExtra("uid", uid)
            context.startActivity(intent)
        }
    }

    override fun getItemCount(): Int {
        return postList.size
    }

    inner class MyHolder(v: View) : RecyclerView.ViewHolder(v) {
        val uPictureIv = v.findViewById<ImageView>(R.id.uPictureIv)
        val pImageIv = v.findViewById<ImageView>(R.id.pImageIv)
        val uNameTv = v.findViewById<TextView>(R.id.uNameTv)
        val pTimeTv = v.findViewById<TextView>(R.id.pTimeTv)
        val pTitleTv = v.findViewById<TextView>(R.id.pTitleTv)
        val pDescriptionTv = v.findViewById<TextView>(R.id.pDescriptionTv)
        val pLikesTv = v.findViewById<TextView>(R.id.pLikesTv)
        val moreBtn = v.findViewById<ImageButton>(R.id.moreBtn)
        val likeBtn = v.findViewById<Button>(R.id.likeBtn)
        val profileLayout = v.findViewById<LinearLayout>(R.id.profileLayout)
    }

    fun showMoreOptions(
        moreBtn: ImageButton,
        uid: String,
        myUid: String,
        pId: String,
        pImage: String
    ) {
        var popupmenu = PopupMenu(context, moreBtn, Gravity.END)
        if (uid.equals(myUid)) {
            popupmenu.menu.add(Menu.NONE, 0, 0, "Delete")
        }

        popupmenu.setOnMenuItemClickListener(object : PopupMenu.OnMenuItemClickListener {
            override fun onMenuItemClick(v: MenuItem?): Boolean {
                var id = v?.itemId
                if (id == 0) {
                    beginDelete(pId, pImage)
                }
                return false
            }

        })
        popupmenu.show()
    }

    fun beginDelete(pId: String, pImage: String) {
        if (pImage.equals("noImage")) {
            deleteWithoutImage(pId)
        } else {
            deleteWithImage(pId, pImage)
        }
    }

    fun deleteWithoutImage(pId: String) {
        var pd = ProgressDialog(context)
        pd.setMessage("Deleting")
        var qu =
            FirebaseDatabase.getInstance().getReference("Posts").orderByChild("pId").equalTo(pId)
        qu.addListenerForSingleValueEvent(object : ValueEventListener {
            override fun onDataChange(snapshot: DataSnapshot) {
                for (ds in snapshot.children) {
                    ds.ref.removeValue()
                }
                Toast.makeText(context, "Deleted successfully", Toast.LENGTH_SHORT).show()
                pd.dismiss()
            }

            override fun onCancelled(error: DatabaseError) {

            }

        })
    }

    fun deleteWithImage(pId: String, pImage: String) {
        var pd = ProgressDialog(context)
        pd.setMessage("Deleting")

        var picRef = FirebaseStorage.getInstance().getReferenceFromUrl(pImage)
        picRef.delete()
            .addOnCompleteListener {
                var qu = FirebaseDatabase.getInstance().getReference("Posts").orderByChild("pId")
                    .equalTo(pId)
                qu.addListenerForSingleValueEvent(object : ValueEventListener {
                    override fun onDataChange(snapshot: DataSnapshot) {
                        for (ds in snapshot.children) {
                            ds.ref.removeValue()
                        }
                        Toast.makeText(context, "Deleted successfully", Toast.LENGTH_SHORT).show()
                        pd.dismiss()
                    }

                    override fun onCancelled(error: DatabaseError) {

                    }

                })
            }
            .addOnFailureListener {

            }
    }
}