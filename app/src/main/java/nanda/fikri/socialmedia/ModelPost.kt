package nanda.fikri.socialmedia

class ModelPost {

    var pId: String = ""
        get() = field
        set(value) {
            field = value
        }

    var pTitle: String = ""
        get() = field
        set(value) {
            field = value
        }

    var pDesc: String = ""
        get() = field
        set(value) {
            field = value
        }

    var pImage: String = ""
        get() = field
        set(value) {
            field = value
        }

    var pTime: String = ""
        get() = field
        set(value) {
            field = value
        }

    var uid: String = ""
        get() = field
        set(value) {
            field = value
        }

    var uEmail: String = ""
        get() = field
        set(value) {
            field = value
        }

    var uDp: String = ""
        get() = field
        set(value) {
            field = value
        }

    var uName: String = ""
        get() = field
        set(value) {
            field = value
        }

    constructor()
    constructor(
        pId: String,
        pTitle: String,
        pDesc: String,
        pImage: String,
        pTime: String,
        uid: String,
        uEmail: String,
        uDp: String,
        uName: String
    ) {
        this.pId = pId
        this.pTitle = pTitle
        this.pDesc = pDesc
        this.pImage = pImage
        this.pTime = pTime
        this.uid = uid
        this.uEmail = uEmail
        this.uDp = uDp
        this.uName = uName
    }
}