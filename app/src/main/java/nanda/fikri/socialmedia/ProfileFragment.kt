package nanda.fikri.socialmedia

import android.Manifest
import android.app.Activity.RESULT_OK
import android.app.AlertDialog
import android.app.ProgressDialog
import android.content.ContentValues
import android.content.DialogInterface
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import android.text.TextUtils
import android.view.*
import android.widget.*
import androidx.appcompat.widget.SearchView
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.database.*
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.StorageReference
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.fragment_profile.*
import java.util.*
import kotlin.collections.HashMap


class ProfileFragment : Fragment() {

    lateinit var v: View
    var auth: FirebaseAuth? = null
    var user: FirebaseUser? = null
    var db: FirebaseDatabase? = null
    var dbref: DatabaseReference? = null
    lateinit var builder: AlertDialog.Builder
    lateinit var progressDialog: ProgressDialog
    var CAMERA_REQUEST_CODE = 100
    var STORAGE_REQUEST_CODE = 200
    var IMAGE_PICK_GALLERY_REQUEST_CODE = 300
    var IMAGE_PICK_CAMERA_REQUEST_CODE = 400
    var cameraPermissions: Array<String> = Array(5) { "" }
    var storagePermissions: Array<String> = Array(5) { "" }
    var image_uri: Uri? = null
    var values: ContentValues? = null
    var profileOrCoverPhoto: String? = null
    var stref: StorageReference? = null
    var storagePath = "Users_Profile_Cover_Imgs/"
    lateinit var hashmap: HashMap<String, Any>
    var adapterPosts3: AdapterPosts? = null
    lateinit var postList: ArrayList<ModelPost>
    var uid: String? = null
    lateinit var rcd: RecyclerView

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        v = inflater.inflate(R.layout.fragment_profile, container, false)
        auth = FirebaseAuth.getInstance()
        user = auth?.currentUser
        db = FirebaseDatabase.getInstance()
        dbref = db?.getReference("Users")

        var query = dbref?.orderByChild("email")?.equalTo(user?.email)
        query?.addValueEventListener(object : ValueEventListener {
            override fun onDataChange(snapshot: DataSnapshot) {

                for (ds in snapshot.children) {
                    var name = "" + ds.child("name").getValue()
                    var email = "" + ds.child("email").getValue()
                    var phone = "" + ds.child("phone").getValue()
                    var image = "" + ds.child("image").getValue()
                    var cover = "" + ds.child("cover").getValue()

                    var n = v.findViewById<TextView>(R.id.nameTv)
                    var e = v.findViewById<TextView>(R.id.emailTv)
                    var p = v.findViewById<TextView>(R.id.phoneTv)
                    var av = v.findViewById<ImageView>(R.id.avatarIv)
                    var co = v.findViewById<ImageView>(R.id.coverIv)

                    n.setText(name)
                    e.setText(email)
                    p.setText(phone)

                    try {
                        Picasso.get().load(image).into(av)
                    } catch (e: Exception) {
                        Picasso.get().load(R.drawable.ic_add_image).into(av)
                    }

                    try {
                        Picasso.get().load(cover).into(co)
                    } catch (e: Exception) {

                    }
                }

            }

            override fun onCancelled(error: DatabaseError) {

            }
        })

        progressDialog = ProgressDialog(context)
        builder = AlertDialog.Builder(context)

        cameraPermissions =
            arrayOf(Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE)
        storagePermissions = arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE)
        hashmap = HashMap()
        values = ContentValues()
        postList = ArrayList()

        stref = FirebaseStorage.getInstance().reference

        rcd = v.findViewById<RecyclerView>(R.id.recyclerview_posts)
        checkUserStatus()
        loadMyPosts()

        return v
    }

    fun loadMyPosts() {
        var li = LinearLayoutManager(context)
        li.stackFromEnd = true
        li.reverseLayout = true
        rcd.layoutManager = li

        var ref = FirebaseDatabase.getInstance().getReference("Posts")
        var query = ref.orderByChild("uid").equalTo(uid)
        query.addValueEventListener(object : ValueEventListener {
            override fun onDataChange(snapshot: DataSnapshot) {
                postList.clear()
                for (ds in snapshot.children) {
                    var myPosts = ds.getValue(ModelPost::class.java)
                    postList.add(myPosts!!)
                    adapterPosts3 = AdapterPosts(context!!, postList)
                    rcd.adapter = adapterPosts3
                }

            }

            override fun onCancelled(error: DatabaseError) {

            }

        })

    }

    fun searchMyPosts(query1: String) {
        var li = LinearLayoutManager(context)
        li.stackFromEnd = true
        li.reverseLayout = true
        rcd.layoutManager = li

        var ref = FirebaseDatabase.getInstance().getReference("Posts")
        var query = ref.orderByChild("uid").equalTo(uid)
        query.addValueEventListener(object : ValueEventListener {
            override fun onDataChange(snapshot: DataSnapshot) {
                postList.clear()
                for (ds in snapshot.children) {
                    var myPosts = ds.getValue(ModelPost::class.java)

                    if (myPosts?.pTitle?.toLowerCase()!!
                            .contains(query1.toLowerCase()) || myPosts?.pDesc?.toLowerCase()!!
                            .contains(query1.toLowerCase())
                    ) {
                        postList.add(myPosts!!)
                    }
                    adapterPosts3 = AdapterPosts(context!!, postList)
                    rcd.adapter = adapterPosts3
                }
            }

            override fun onCancelled(error: DatabaseError) {
                Toast.makeText(context, "" + error.message, Toast.LENGTH_SHORT).show()
            }

        })

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        fab.setOnClickListener {
            showEditProfileDialog()
        }
    }

    fun showEditProfileDialog() {
        val options = arrayOf("Edit Profile Picture", "Edit Cover Photo", "Edit Name", "Edit Phone")
        builder.setTitle("Choose Action")
        builder.setItems(options, DialogInterface.OnClickListener { dialogInterface, i ->
            if (i == 0) {
                progressDialog.setMessage("Updating Profile Picture")
                profileOrCoverPhoto = "image"
                showImagePicDialog()
            } else if (i == 1) {
                progressDialog.setMessage("Updating Cover Photo")
                profileOrCoverPhoto = "cover"
                showImagePicDialog()
            } else if (i == 2) {
                progressDialog.setMessage("Updating Name")
                showNamePhoneUpdateDialog("name")
            } else if (i == 3) {
                progressDialog.setMessage("Updating Phone")
                showNamePhoneUpdateDialog("phone")
            }
        })
        builder.create().show()
    }

    fun showImagePicDialog() {
        var c = AlertDialog.Builder(context)
        val options = arrayOf("Camera", "Gallery")
        c.setTitle("Pick Image From")
        c.setItems(options, DialogInterface.OnClickListener { dialogInterface, i ->
            if (i == 0) {
                if (!checkCameraPermission()) {
                    requestCameraPermission()
                } else {
                    pickFromCamera()
                }
            } else if (i == 1) {
                if (!checkStoragePermission()) {
                    requestStoragePermission()
                } else {
                    pickFromGallery()
                }
            }
        })
        c.create().show()
    }

    fun checkStoragePermission(): Boolean {
        var result =
            ContextCompat.checkSelfPermission(
                context!!,
                Manifest.permission.WRITE_EXTERNAL_STORAGE
            ) == (PackageManager.PERMISSION_GRANTED)
        return result
    }

    fun requestStoragePermission() {
        requestPermissions(storagePermissions, STORAGE_REQUEST_CODE)
    }

    fun checkCameraPermission(): Boolean {
        var result =
            ContextCompat.checkSelfPermission(
                context!!,
                Manifest.permission.CAMERA
            ) == (PackageManager.PERMISSION_GRANTED)

        var result1 =
            ContextCompat.checkSelfPermission(
                context!!,
                Manifest.permission.WRITE_EXTERNAL_STORAGE
            ) == (PackageManager.PERMISSION_GRANTED)
        return result && result1
    }

    fun requestCameraPermission() {
        requestPermissions(cameraPermissions, CAMERA_REQUEST_CODE)
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        when (requestCode) {
            CAMERA_REQUEST_CODE -> {
                if (grantResults.size > 0) {
                    var cameraAccepted = grantResults[0] == PackageManager.PERMISSION_GRANTED
                    var writeStorageAccepted = grantResults[1] == PackageManager.PERMISSION_GRANTED
                    if (cameraAccepted && writeStorageAccepted) {
                        pickFromCamera()
                    } else {
                        Toast.makeText(
                            context!!,
                            "Please enable camera & storage permission",
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                }
            }
            STORAGE_REQUEST_CODE -> {
                if (grantResults.size > 0) {
                    var writeStorageAccepted = grantResults[1] == PackageManager.PERMISSION_GRANTED
                    if (writeStorageAccepted) {
                        pickFromGallery()
                    } else {
                        Toast.makeText(
                            context!!,
                            "Please enable storage permission",
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                }
            }
        }

        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
    }

    fun pickFromCamera() {
        values?.put(MediaStore.Images.Media.TITLE, "Temp Pic")
        values?.put(MediaStore.Images.Media.DESCRIPTION, "Temp Description")
        image_uri =
            context!!.contentResolver?.insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values)
        var cameraIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, image_uri)
        startActivityForResult(cameraIntent, IMAGE_PICK_CAMERA_REQUEST_CODE)
    }

    fun pickFromGallery() {
        var galleryIntent = Intent(Intent.ACTION_PICK)
        galleryIntent.setType("image/*")
        startActivityForResult(galleryIntent, IMAGE_PICK_GALLERY_REQUEST_CODE)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (resultCode == RESULT_OK) {
            if (requestCode == IMAGE_PICK_GALLERY_REQUEST_CODE) {
                image_uri = data?.data
                uploadProfileCoverPhoto(image_uri)
            }
            if (requestCode == IMAGE_PICK_CAMERA_REQUEST_CODE) {
                uploadProfileCoverPhoto(image_uri)
            }
        }
        super.onActivityResult(requestCode, resultCode, data)
    }

    fun uploadProfileCoverPhoto(uri: Uri?) {
        progressDialog.show()
        var filePathAndName = storagePath + "" + profileOrCoverPhoto + "_" + user?.uid
        var storageReference2nd = stref?.child(filePathAndName)
        storageReference2nd?.putFile(uri!!)!!
            .addOnSuccessListener {

                val result = it.metadata!!.reference!!.downloadUrl;
                result.addOnSuccessListener {

                    hashmap.put(profileOrCoverPhoto!!, it.toString())
                    dbref?.child(user!!.uid)!!.updateChildren(hashmap)
                        .addOnSuccessListener {
                            progressDialog.dismiss()
                            Toast.makeText(context!!, "Image Updated...", Toast.LENGTH_SHORT)
                                .show()
                        }
                        .addOnFailureListener {
                            progressDialog.dismiss()
                            Toast.makeText(
                                context!!,
                                "Error Updating Image...",
                                Toast.LENGTH_SHORT
                            ).show()
                        }
                    if (profileOrCoverPhoto.equals("image")) {
                        var ref = FirebaseDatabase.getInstance().getReference("Posts")
                        var query = ref.orderByChild("uid").equalTo(uid)
                        query.addValueEventListener(object : ValueEventListener {
                            override fun onDataChange(snapshot: DataSnapshot) {
                                for (ds in snapshot.children) {
                                    var child = ds.key
                                    snapshot.ref.child(child!!).child("uDp").setValue(it.toString())
                                }
                            }

                            override fun onCancelled(error: DatabaseError) {

                            }

                        })
                    }
                }

            }
            .addOnFailureListener {
                progressDialog.dismiss()
                Toast.makeText(context!!, "" + it.message, Toast.LENGTH_SHORT).show()
            }

    }

    fun showNamePhoneUpdateDialog(key: String) {
        var b = AlertDialog.Builder(context)
        b.setTitle("Update " + key)
        var linearLayout = LinearLayout(context!!)
        linearLayout.orientation = LinearLayout.VERTICAL
        linearLayout.setPadding(10, 10, 10, 10)
        var editText = EditText(context!!)
        editText.setHint("Enter " + key)
        linearLayout.addView(editText)

        b.setView(linearLayout)
        b.setPositiveButton("Update", DialogInterface.OnClickListener { dialogInterface, i ->
            var value = editText.text.toString().trim()
            if (!TextUtils.isEmpty(value)) {
                progressDialog.show()
                hashmap.put(key, value)
                dbref?.child(user!!.uid)!!.updateChildren(hashmap)
                    .addOnCompleteListener {
                        progressDialog.dismiss()
                        Toast.makeText(context!!, "Updated", Toast.LENGTH_SHORT).show()
                    }
                    .addOnFailureListener {
                        progressDialog.dismiss()
                        Toast.makeText(context!!, "" + it.message, Toast.LENGTH_SHORT).show()
                    }
                if (key.equals("name")) {
                    var ref = FirebaseDatabase.getInstance().getReference("Posts")
                    var query = ref.orderByChild("uid").equalTo(uid)
                    query.addValueEventListener(object : ValueEventListener {
                        override fun onDataChange(snapshot: DataSnapshot) {
                            for (ds in snapshot.children) {
                                var child = ds.key
                                snapshot.ref.child(child!!).child("uName").setValue(value)
                            }
                        }

                        override fun onCancelled(error: DatabaseError) {

                        }

                    })
                }
            } else {
                Toast.makeText(context!!, "Please enter " + key, Toast.LENGTH_SHORT).show()
            }
        })
        b.setNegativeButton("Cancel", DialogInterface.OnClickListener { dialogInterface, i ->
            dialogInterface.dismiss()
        })
        b.create().show()
    }

    fun checkUserStatus() {
        var user = auth?.currentUser
        if (user != null) {
//            profileTV.setText(user.email)
            uid = user.uid
        } else {
            startActivity(Intent(context, MainActivity::class.java))
            activity?.finish()
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        setHasOptionsMenu(true)
        super.onCreate(savedInstanceState)
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.menu_main, menu)
        menu.findItem(R.id.action_add_post).setVisible(false)
        var item = menu.findItem(R.id.action_search)
        var searchView = item.actionView as SearchView
        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                if (!TextUtils.isEmpty(query?.trim())) {
                    searchMyPosts(query!!)
                } else {
                    loadMyPosts()
                }
                return false
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                if (!TextUtils.isEmpty(newText?.trim())) {
                    searchMyPosts(newText!!)
                } else {
                    loadMyPosts()
                }
                return false
            }

        })
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.action_logout -> {
                auth?.signOut()
                checkUserStatus()
            }
            R.id.action_add_post -> {
                startActivity(Intent(context, AddPostActivity::class.java))
            }
        }
        return super.onOptionsItemSelected(item)
    }
}