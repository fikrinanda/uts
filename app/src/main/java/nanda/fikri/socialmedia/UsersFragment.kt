package nanda.fikri.socialmedia

import android.content.Intent
import android.os.Bundle
import android.text.TextUtils
import android.view.*
import androidx.appcompat.widget.SearchView
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import java.util.*

class UsersFragment : Fragment() {

    lateinit var v: View
    var adapterUsers: AdapterUsers? = null
    lateinit var userList: ArrayList<ModelUser>
    lateinit var rc: RecyclerView
    lateinit var firebaseAuth: FirebaseAuth

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        v = inflater.inflate(R.layout.fragment_users, container, false)
        rc = v.findViewById<RecyclerView>(R.id.users_recyclerView)
        rc.setHasFixedSize(true)
        rc.layoutManager = LinearLayoutManager(context)

        userList = ArrayList()
        getAllUsers()

        firebaseAuth = FirebaseAuth.getInstance()

        return v
    }

    fun getAllUsers() {
        var fUser = FirebaseAuth.getInstance().currentUser
        var ref = FirebaseDatabase.getInstance().getReference("Users")
        ref.addValueEventListener(object : ValueEventListener {
            override fun onDataChange(snapshot: DataSnapshot) {
                userList.clear()
                for (ds in snapshot.children) {

                    var modelUser = ds.getValue(ModelUser::class.java)

                    if (!modelUser?.uid.equals(fUser?.uid)) {
                        userList.add(modelUser!!)
                    }
                    adapterUsers = AdapterUsers(context!!, userList)
                    rc.adapter = adapterUsers

                }
            }

            override fun onCancelled(error: DatabaseError) {

            }

        })
    }

    fun searchUser(query: String) {
        var fUser = FirebaseAuth.getInstance().currentUser
        var ref = FirebaseDatabase.getInstance().getReference("Users")
        ref.addValueEventListener(object : ValueEventListener {
            override fun onDataChange(snapshot: DataSnapshot) {
                userList.clear()
                for (ds in snapshot.children) {

                    var modelUser = ds.getValue(ModelUser::class.java)

                    if (!modelUser?.uid.equals(fUser?.uid)) {
                        if (modelUser?.name?.toLowerCase()!!
                                .contains(query.toLowerCase()) || modelUser?.email?.toLowerCase()!!
                                .contains(query.toLowerCase())
                        ) {
                            userList.add(modelUser!!)
                        }

                    }
                    adapterUsers = AdapterUsers(context!!, userList)
                    adapterUsers?.notifyDataSetChanged()
                    rc.adapter = adapterUsers

                }
            }

            override fun onCancelled(error: DatabaseError) {

            }

        })
    }

    fun checkUserStatus() {
        var user = firebaseAuth.currentUser
        if (user != null) {
//            profileTV.setText(user.email)
        } else {
            startActivity(Intent(context, MainActivity::class.java))
            activity?.finish()
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        setHasOptionsMenu(true)
        super.onCreate(savedInstanceState)
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.menu_main, menu)
        menu.findItem(R.id.action_add_post).setVisible(false)

        var item = menu.findItem(R.id.action_search)
        var searchView = item.actionView as SearchView
        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                if (!TextUtils.isEmpty(query?.trim())) {
                    searchUser(query!!)
                } else {
                    getAllUsers()
                }
                return false
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                if (!TextUtils.isEmpty(newText?.trim())) {
                    searchUser(newText!!)
                } else {
                    getAllUsers()
                }
                return false
            }

        })

        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.action_logout -> {
                firebaseAuth.signOut()
                checkUserStatus()
            }
        }
        return super.onOptionsItemSelected(item)
    }
}