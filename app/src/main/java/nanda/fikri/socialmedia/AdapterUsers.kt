package nanda.fikri.socialmedia

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso

class AdapterUsers : RecyclerView.Adapter<AdapterUsers.MyHolder> {

    var context: Context
    var userList: List<ModelUser>

    constructor(context: Context, userList: List<ModelUser>) {
        this.context = context
        this.userList = userList
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyHolder {
        val v = LayoutInflater.from(context).inflate(R.layout.row_users, parent, false)
        return MyHolder(v)
    }

    override fun onBindViewHolder(holder: MyHolder, position: Int) {
        var userImage = userList.get(position).image
        var userName = userList.get(position).name
        var userEmail = userList.get(position).email
        var hisUid = userList.get(position).uid

        holder.mNameTv.setText(userName)
        holder.mEmailTv.setText(userEmail)
        try {
            Picasso.get().load(userImage).placeholder(R.drawable.ic_default_img)
                .into(holder.mAvatarIv)
        } catch (e: Exception) {

        }

        holder.itemView.setOnClickListener {
            var intent = Intent(context, ThereProfileActivity::class.java)
            intent.putExtra("uid", hisUid)
            context.startActivity(intent)
        }
    }

    override fun getItemCount(): Int {
        return userList.size
    }

    inner class MyHolder(v: View) : RecyclerView.ViewHolder(v) {
        val mAvatarIv = v.findViewById<ImageView>(R.id.avatarIv)
        val mNameTv = v.findViewById<TextView>(R.id.nameTv)
        val mEmailTv = v.findViewById<TextView>(R.id.emailTv)
    }

}