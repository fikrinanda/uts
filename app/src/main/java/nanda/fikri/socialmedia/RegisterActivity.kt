package nanda.fikri.socialmedia

import android.app.ProgressDialog
import android.content.Intent
import android.os.Bundle
import android.util.Patterns
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.ActionBar
import androidx.appcompat.app.AppCompatActivity
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.database.FirebaseDatabase
import kotlinx.android.synthetic.main.activity_register.*

class RegisterActivity : AppCompatActivity(), View.OnClickListener {

    lateinit var progressDialog: ProgressDialog
    var user: FirebaseUser? = null
    lateinit var database: FirebaseDatabase
    lateinit var hashmap: HashMap<String, String>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)

        val actionBar: ActionBar? = this.supportActionBar
        actionBar?.setTitle("Create Account")
        actionBar?.setDisplayHomeAsUpEnabled(true)
        actionBar?.setDisplayShowHomeEnabled(true)

        progressDialog = ProgressDialog(this)
        progressDialog.setMessage("Registering User...")

        registerBtn.setOnClickListener(this)
        have_accountTv.setOnClickListener(this)

        database = FirebaseDatabase.getInstance()
        hashmap = HashMap()
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return super.onSupportNavigateUp()
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.registerBtn -> {
                var email = emailEt.text.toString().trim()
                var password = passwordEt.text.toString().trim()

                if (!Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
                    emailEt.setError("Invalid Email")
                    emailEt.isFocusable
                } else if (password.length < 6) {
                    passwordEt.setError("Password length at least 6 characters")
                    passwordEt.isFocusable
                } else {
                    registerUser(email, password)
                }
            }
            R.id.have_accountTv -> {
                startActivity(Intent(this, LoginActivity::class.java))
                finish()
            }
        }
    }

    fun registerUser(email: String, password: String) {
        progressDialog.show()

        FirebaseAuth.getInstance().createUserWithEmailAndPassword(email, password)
            .addOnCompleteListener {
                if (it.isSuccessful) {
                    progressDialog.dismiss()
                    user = FirebaseAuth.getInstance().currentUser
                    var email = user?.email
                    var uid = user?.uid

                    hashmap.set("email", email.toString())
                    hashmap.set("uid", uid.toString())
                    hashmap.set("name", "")
                    hashmap.set("phone", "")
                    hashmap.set("image", "")
                    hashmap.set("cover", "")

                    val reference = database.getReference("Users")
                    reference.child(uid.toString()).setValue(hashmap)

                    Toast.makeText(this, "Registered...\n" + user!!.email, Toast.LENGTH_SHORT)
                        .show()
                    startActivity(Intent(this, DashboardActivity::class.java))
                    finish()
                } else {
                    progressDialog.dismiss()
                    Toast.makeText(this, "Authentication failed", Toast.LENGTH_SHORT).show()
                }
            }.addOnFailureListener {
                progressDialog.dismiss()
                Toast.makeText(this, "" + it.message, Toast.LENGTH_SHORT).show()
            }
    }
}