package nanda.fikri.socialmedia

import android.content.Intent
import android.os.Bundle
import android.text.TextUtils
import android.view.*
import androidx.appcompat.widget.SearchView
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import java.util.*

class HomeFragment : Fragment() {

    lateinit var v: View
    lateinit var firebaseAuth: FirebaseAuth
    var adapterPosts2: AdapterPosts? = null
    lateinit var postList: ArrayList<ModelPost>
    lateinit var rcc: RecyclerView
    var user: FirebaseUser? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        firebaseAuth = FirebaseAuth.getInstance()
        user = firebaseAuth?.currentUser
        v = inflater.inflate(R.layout.fragment_home, container, false)
        rcc = v.findViewById<RecyclerView>(R.id.postsRecyclerview)
        rcc.setHasFixedSize(true)
        var li = LinearLayoutManager(context)
        rcc.layoutManager = li
        li.stackFromEnd = true
        li.reverseLayout = true

        postList = ArrayList()

        loadPosts()

        firebaseAuth = FirebaseAuth.getInstance()

        return v
    }

    fun checkUserStatus() {
        var user = firebaseAuth.currentUser
        if (user != null) {
//            profileTV.setText(user.email)
        } else {
            startActivity(Intent(context, MainActivity::class.java))
            activity?.finish()
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        setHasOptionsMenu(true)
        super.onCreate(savedInstanceState)
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.menu_main, menu)

        var item = menu.findItem(R.id.action_search)
        var searchView = item.actionView as SearchView
        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                if (!TextUtils.isEmpty(query?.trim())) {
                    searchPosts(query!!)
                } else {
                    loadPosts()
                }
                return false
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                if (!TextUtils.isEmpty(newText?.trim())) {
                    searchPosts(newText!!)
                } else {
                    loadPosts()
                }
                return false
            }

        })

        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.action_logout -> {
                firebaseAuth.signOut()
                checkUserStatus()
            }
            R.id.action_add_post -> {
                startActivity(Intent(context, AddPostActivity::class.java))
            }
        }
        return super.onOptionsItemSelected(item)
    }

    fun loadPosts() {
        var ref = FirebaseDatabase.getInstance().getReference("Posts")
        ref.addValueEventListener(object : ValueEventListener {
            override fun onDataChange(snapshot: DataSnapshot) {
                postList.clear()
                for (ds in snapshot.children) {
                    var modelPost = ds.getValue(ModelPost::class.java)
                    postList.add(modelPost!!)
                    adapterPosts2 = AdapterPosts(context!!, postList)
                    rcc.adapter = adapterPosts2
                }


            }

            override fun onCancelled(error: DatabaseError) {

            }

        })
    }

    fun searchPosts(query: String) {
        var fUser = FirebaseAuth.getInstance().currentUser
        var ref = FirebaseDatabase.getInstance().getReference("Posts")
        ref.addValueEventListener(object : ValueEventListener {
            override fun onDataChange(snapshot: DataSnapshot) {
                postList.clear()
                for (ds in snapshot.children) {

                    var modelPost = ds.getValue(ModelPost::class.java)

                    if (modelPost?.pTitle?.toLowerCase()!!
                            .contains(query.toLowerCase()) || modelPost?.pDesc?.toLowerCase()!!
                            .contains(query.toLowerCase())
                    ) {
                        postList.add(modelPost!!)
                    }

                    adapterPosts2 = AdapterPosts(context!!, postList)
                    adapterPosts2?.notifyDataSetChanged()
                    rcc.adapter = adapterPosts2

                }
            }

            override fun onCancelled(error: DatabaseError) {

            }

        })
    }
}