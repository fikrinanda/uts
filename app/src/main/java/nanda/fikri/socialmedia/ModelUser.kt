package nanda.fikri.socialmedia

class ModelUser {

    var name: String = ""
        get() = field
        set(value) {
            field = value
        }

    var email: String = ""
        get() = field
        set(value) {
            field = value
        }

    var search: String = ""
        get() = field
        set(value) {
            field = value
        }

    var phone: String = ""
        get() = field
        set(value) {
            field = value
        }

    var image: String = ""
        get() = field
        set(value) {
            field = value
        }

    var cover: String = ""
        get() = field
        set(value) {
            field = value
        }

    var uid: String = ""
        get() = field
        set(value) {
            field = value
        }

    constructor()
    constructor(
        name: String,
        email: String,
        search: String,
        phone: String,
        image: String,
        cover: String,
        uid: String
    ) {
        this.name = name
        this.email = email
        this.search = search
        this.phone = phone
        this.image = image
        this.cover = cover
        this.uid = uid
    }

}