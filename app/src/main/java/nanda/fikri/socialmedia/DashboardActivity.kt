package nanda.fikri.socialmedia

import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import androidx.appcompat.app.ActionBar
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.FragmentTransaction
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.activity_dashboard.*

class DashboardActivity : AppCompatActivity(),
    BottomNavigationView.OnNavigationItemSelectedListener {

    lateinit var firebaseAuth: FirebaseAuth
    lateinit var ft: FragmentTransaction
    lateinit var fragHome: HomeFragment
    lateinit var fragProfile: ProfileFragment
    lateinit var fragUsers: UsersFragment
    var actionBar: ActionBar? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_dashboard)

        actionBar = this.supportActionBar

        navigation.setOnNavigationItemSelectedListener(this)
        fragHome = HomeFragment()
        fragProfile = ProfileFragment()
        fragUsers = UsersFragment()

        firebaseAuth = FirebaseAuth.getInstance()

        actionBar?.setTitle("Home")
        ft = supportFragmentManager.beginTransaction()
        ft.replace(R.id.content, fragHome).commit()
    }

    fun checkUserStatus() {
        var user = firebaseAuth.currentUser
        if (user != null) {
//            profileTV.setText(user.email)
        } else {
            startActivity(Intent(this, MainActivity::class.java))
            finish()
        }
    }

    override fun onStart() {
        checkUserStatus()
        super.onStart()
    }

    override fun onBackPressed() {
        super.onBackPressed()
        finish()
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.nav_home -> {
                actionBar?.setTitle("Home")
                ft = supportFragmentManager.beginTransaction()
                ft.replace(R.id.content, fragHome).commit()
                return true
            }
            R.id.nav_profile -> {
                actionBar?.setTitle("Profile")
                ft = supportFragmentManager.beginTransaction()
                ft.replace(R.id.content, fragProfile).commit()
                return true
            }
            R.id.nav_users -> {
                actionBar?.setTitle("Users")
                ft = supportFragmentManager.beginTransaction()
                ft.replace(R.id.content, fragUsers).commit()
                return true
            }
        }
        return false
    }
}