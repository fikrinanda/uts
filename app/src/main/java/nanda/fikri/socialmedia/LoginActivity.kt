package nanda.fikri.socialmedia

import android.app.AlertDialog
import android.app.ProgressDialog
import android.content.DialogInterface
import android.content.Intent
import android.os.Bundle
import android.text.InputType
import android.util.Patterns
import android.view.View
import android.widget.EditText
import android.widget.LinearLayout
import android.widget.Toast
import androidx.appcompat.app.ActionBar
import androidx.appcompat.app.AppCompatActivity
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.api.ApiException
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.auth.GoogleAuthProvider
import com.google.firebase.database.FirebaseDatabase
import kotlinx.android.synthetic.main.activity_login.*

class LoginActivity : AppCompatActivity(), View.OnClickListener {

    lateinit var progressDialog: ProgressDialog
    var user: FirebaseUser? = null
    lateinit var builder: AlertDialog.Builder
    lateinit var linearLayout: LinearLayout
    lateinit var email: EditText
    lateinit var googleSignInClient: GoogleSignInClient
    val RC_SIGN_IN = 100
    private lateinit var auth: FirebaseAuth
    lateinit var database: FirebaseDatabase

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        val actionBar: ActionBar? = this.supportActionBar
        actionBar?.setTitle("Login")
        actionBar?.setDisplayHomeAsUpEnabled(true)
        actionBar?.setDisplayShowHomeEnabled(true)

        progressDialog = ProgressDialog(this)

        builder = AlertDialog.Builder(this)
        linearLayout = LinearLayout(this)
        email = EditText(this)

        loginBtn.setOnClickListener(this)
        donthave_accountTv.setOnClickListener(this)
        recoverPassTv.setOnClickListener(this)
        googleLoginBtn.setOnClickListener(this)

        val gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
            .requestIdToken(getString(R.string.default_web_client_id))
            .requestEmail()
            .build()
        googleSignInClient = GoogleSignIn.getClient(this, gso)

        auth = FirebaseAuth.getInstance()
        database = FirebaseDatabase.getInstance()
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.loginBtn -> {
                var email = emailEt.text.toString().trim()
                var password = passwordEt.text.toString().trim()
                if (!Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
                    emailEt.setError("Invalid Email")
                    emailEt.isFocusable
                } else {
                    loginUser(email, password)
                }
            }
            R.id.donthave_accountTv -> {
                startActivity(Intent(this, RegisterActivity::class.java))
                finish()
            }
            R.id.recoverPassTv -> {
                showRecoverPasswordDialog()
            }
            R.id.googleLoginBtn -> {
                val signInIntent = googleSignInClient.signInIntent
                startActivityForResult(signInIntent, RC_SIGN_IN)
            }

        }
    }

    fun loginUser(email: String, password: String) {
        progressDialog.setMessage("Logging In...")
        progressDialog.show()

        FirebaseAuth.getInstance().signInWithEmailAndPassword(email, password)
            .addOnCompleteListener {
                if (it.isSuccessful) {
                    progressDialog.dismiss()
                    user = FirebaseAuth.getInstance().currentUser
                    startActivity(Intent(this, DashboardActivity::class.java))
                    finish()
                } else {
                    progressDialog.dismiss()
                    Toast.makeText(this, "Authentication failed", Toast.LENGTH_SHORT).show()
                }
            }
            .addOnFailureListener {
                progressDialog.dismiss()
                Toast.makeText(this, "" + it.message, Toast.LENGTH_SHORT).show()
            }
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return super.onSupportNavigateUp()
    }

    fun showRecoverPasswordDialog() {
        builder.setTitle("Recover Password")
        email.setHint("Email")
        email.inputType = InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS
        email.minEms = 16
        linearLayout.addView(email)
        linearLayout.setPadding(10, 10, 10, 10)
        builder.setView(linearLayout)

        builder.setPositiveButton("Recover", DialogInterface.OnClickListener { dialogInterface, i ->
            var email2 = email.text.toString().trim()
            beginRecovery(email2)
        })
        builder.setNegativeButton("Cancel", DialogInterface.OnClickListener { dialogInterface, i ->
            dialogInterface.dismiss()
        })

        builder.create().show()
    }

    fun beginRecovery(email2: String) {
        progressDialog.setMessage("Sending email...")
        progressDialog.show()
        FirebaseAuth.getInstance().sendPasswordResetEmail(email2)
            .addOnCompleteListener {
                if (it.isSuccessful) {
                    progressDialog.dismiss()
                    Toast.makeText(this, "Email sent", Toast.LENGTH_SHORT).show()
                } else {
                    progressDialog.dismiss()
                    Toast.makeText(this, "Failed...", Toast.LENGTH_SHORT).show()
                }
            }
            .addOnFailureListener {
                progressDialog.dismiss()
                Toast.makeText(this, "" + it.message, Toast.LENGTH_SHORT).show()
            }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        // Result returned from launching the Intent from GoogleSignInApi.getSignInIntent(...);
        if (requestCode == RC_SIGN_IN) {
            val task = GoogleSignIn.getSignedInAccountFromIntent(data)
            try {
                // Google Sign In was successful, authenticate with Firebase
                val account = task.getResult(ApiException::class.java)!!
                firebaseAuthWithGoogle(account.idToken!!)
            } catch (e: ApiException) {
                // Google Sign In failed, update UI appropriately
                Toast.makeText(this, "" + e.message, Toast.LENGTH_SHORT).show()
                // ...
            }
        }
    }

    private fun firebaseAuthWithGoogle(idToken: String) {
        val credential = GoogleAuthProvider.getCredential(idToken, null)
        auth.signInWithCredential(credential)
            .addOnCompleteListener(this) { task ->
                if (task.isSuccessful) {
                    // Sign in success, update UI with the signed-in user's information
                    val user = auth.currentUser
                    if (task.getResult()?.additionalUserInfo!!.isNewUser) {
                        var email = user?.email
                        var uid = user?.uid
                        val hashmap = HashMap<String, Any>()
                        hashmap.put("email", email.toString())
                        hashmap.put("uid", uid.toString())
                        hashmap.put("name", "")
                        hashmap.put("phone", "")
                        hashmap.put("image", "")
                        hashmap.put("cover", "")

                        val reference = database.getReference("Users")
                        reference.child(uid.toString()).setValue(hashmap)
                    }

                    Toast.makeText(this, "" + user?.email, Toast.LENGTH_SHORT).show()
                    startActivity(Intent(this, DashboardActivity::class.java))
                    finish()
                    //updateUI(user)
                } else {
                    // If sign in fails, display a message to the user.
                    Toast.makeText(this, "Login Failed...", Toast.LENGTH_SHORT).show()
                    //updateUI(null)
                }
            }
            .addOnFailureListener {
                Toast.makeText(this, "" + it.message, Toast.LENGTH_SHORT).show()
            }
    }

}