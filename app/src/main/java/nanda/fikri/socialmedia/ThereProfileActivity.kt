package nanda.fikri.socialmedia

import android.content.Intent
import android.os.Bundle
import android.text.TextUtils
import android.view.Menu
import android.view.MenuItem
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.ActionBar
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.SearchView
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.database.*
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_there_profile.*
import java.util.*

class ThereProfileActivity : AppCompatActivity() {

    var adapterPosts: AdapterPosts? = null
    lateinit var postList: ArrayList<ModelPost>
    var uid: String? = null
    var auth: FirebaseAuth? = null
    var db: FirebaseDatabase? = null
    var dbref: DatabaseReference? = null
    var user: FirebaseUser? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_there_profile)

        val actionBar: ActionBar? = this.supportActionBar
        actionBar?.setTitle("Profile")
        actionBar?.setDisplayHomeAsUpEnabled(true)
        actionBar?.setDisplayShowHomeEnabled(true)

        auth = FirebaseAuth.getInstance()
        user = auth?.currentUser
        db = FirebaseDatabase.getInstance()
        dbref = db?.getReference("Users")

        var intent = intent
        uid = intent.getStringExtra("uid")

        var query = dbref?.orderByChild("uid")?.equalTo(uid)
        query?.addValueEventListener(object : ValueEventListener {
            override fun onDataChange(snapshot: DataSnapshot) {

                for (ds in snapshot.children) {
                    var name = "" + ds.child("name").getValue()
                    var email = "" + ds.child("email").getValue()
                    var phone = "" + ds.child("phone").getValue()
                    var image = "" + ds.child("image").getValue()
                    var cover = "" + ds.child("cover").getValue()

                    var n = findViewById<TextView>(R.id.nameTv)
                    var e = findViewById<TextView>(R.id.emailTv)
                    var p = findViewById<TextView>(R.id.phoneTv)
                    var av = findViewById<ImageView>(R.id.avatarIv)
                    var co = findViewById<ImageView>(R.id.coverIv)

                    n.setText(name)
                    e.setText(email)
                    p.setText(phone)

                    try {
                        Picasso.get().load(image).into(av)
                    } catch (e: Exception) {
                        Picasso.get().load(R.drawable.ic_add_image).into(av)
                    }

                    try {
                        Picasso.get().load(cover).into(co)
                    } catch (e: Exception) {

                    }
                }

            }

            override fun onCancelled(error: DatabaseError) {

            }
        })

        postList = ArrayList()

        checkUserStatus()
        loadHisPosts()

    }

    fun loadHisPosts() {
        var li = LinearLayoutManager(this)
        li.stackFromEnd = true
        li.reverseLayout = true
        recyclerview_posts.layoutManager = li

        var ref = FirebaseDatabase.getInstance().getReference("Posts")
        var query = ref.orderByChild("uid").equalTo(uid)
        query.addValueEventListener(object : ValueEventListener {
            override fun onDataChange(snapshot: DataSnapshot) {
                postList.clear()
                for (ds in snapshot.children) {
                    var myPosts = ds.getValue(ModelPost::class.java)
                    postList.add(myPosts!!)
                    adapterPosts = AdapterPosts(this@ThereProfileActivity, postList)
                    recyclerview_posts.adapter = adapterPosts
                }
            }

            override fun onCancelled(error: DatabaseError) {
                Toast.makeText(this@ThereProfileActivity, "" + error.message, Toast.LENGTH_SHORT)
                    .show()
            }

        })
    }

    fun searchHisPosts(query2: String) {
        var li = LinearLayoutManager(this)
        li.stackFromEnd = true
        li.reverseLayout = true
        recyclerview_posts.layoutManager = li

        var ref = FirebaseDatabase.getInstance().getReference("Posts")
        var query = ref.orderByChild("uid").equalTo(uid)
        query.addValueEventListener(object : ValueEventListener {
            override fun onDataChange(snapshot: DataSnapshot) {
                postList.clear()
                for (ds in snapshot.children) {
                    var myPosts = ds.getValue(ModelPost::class.java)

                    if (myPosts?.pTitle?.toLowerCase()!!
                            .contains(query2.toLowerCase()) || myPosts?.pDesc?.toLowerCase()!!
                            .contains(query2.toLowerCase())
                    ) {
                        postList.add(myPosts!!)
                    }
                    adapterPosts = AdapterPosts(this@ThereProfileActivity, postList)
                    recyclerview_posts.adapter = adapterPosts
                }
            }

            override fun onCancelled(error: DatabaseError) {
                Toast.makeText(this@ThereProfileActivity, "" + error.message, Toast.LENGTH_SHORT)
                    .show()
            }

        })

    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_main, menu)
        menu?.findItem(R.id.action_add_post)?.setVisible(false)

        var item = menu?.findItem(R.id.action_search)
        var searchView = item?.actionView as SearchView
        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                if (!TextUtils.isEmpty(query?.trim())) {
                    searchHisPosts(query!!)
                } else {
                    loadHisPosts()
                }
                return false
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                if (!TextUtils.isEmpty(newText?.trim())) {
                    searchHisPosts(newText!!)
                } else {
                    loadHisPosts()
                }
                return false
            }

        })
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.action_logout -> {
                auth?.signOut()
                checkUserStatus()
            }
            R.id.action_add_post -> {
                startActivity(Intent(this, AddPostActivity::class.java))
            }
        }
        return super.onOptionsItemSelected(item)
    }

    fun checkUserStatus() {
        var user = auth?.currentUser
        if (user != null) {
//            profileTV.setText(user.email)
        } else {
            startActivity(Intent(this, MainActivity::class.java))
            finish()
        }
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return super.onSupportNavigateUp()
    }
}